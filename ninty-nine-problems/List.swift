// http://www.enekoalonso.com/projects/99-swift-problems

class List<T> {
    var value: T
    var nextItem: List<T>?

    convenience init?(_ values: T...) {
        self.init(Array(values))
    }

    init?(_ values: [T]) {
        guard let first = values.first else {
            return nil
        }
        value = first
        nextItem = List(Array(values.suffix(from: 1)))
    }
}

// Problem 01: Find the last element of a linked list.
extension List {
    var last: T? {
        get {
            // 1--1--2--3--nil
            var it: List<T>? = self
            while it!.nextItem != nil {
                it = it!.nextItem
            }
            return it!.value
        }
    }
}

print("Last item -> \(List<Int>(1, 1, 2, 3, 4, 5)!.last!)")

// Problem 02: Find the last but one element of a linked list.
extension List {
    var pennultimate: T? {
        get {
            var it: List<T>? = self
            var val: T?
            while it!.nextItem != nil {
                val = it!.value
                it = it!.nextItem
            }
            return val
        }
    }
}

print("Last but one element -> \(List(1, 1, 2, 3, 5, 8)!.pennultimate!)")

// Problem 03: Find the Kth element of a linked list.
enum ListError: Error {
    case indexOutOfBounds
}

extension List {
    subscript(index: Int) -> T? {
        get {
            var idx = 0
            var it: List<T>? = self
            while idx < index {
                it = it!.nextItem
                idx += 1
            }
            return it!.value
        }

        set {
            var idx = 0
            var it: List<T>? = self
            while idx < index {
                it = it!.nextItem
                idx += 1
            }
            it!.value = newValue!
        }
    }
}

let list = List(1, 1, 2, 3, 5, 8)
print("Index @ 2 -> \(list![2]!) ")

// Problem 04: Find the number of elements of a linked list.
extension List {
    var length: Int {
        var length = 0
        var it: List<T>? = self
        while it != nil {
            it = it!.nextItem
            length += 1
        }
        return length
    }
}

print("Length -> \(List<Int>(1, 1, 2, 3, 5, 8)!.length)" )

// Problem 05: Reverse a linked list.
extension List {
    func reverse() -> List<T> {
        let reversed = self
        for index in 0...((self.length / 2) - 1) {
            // print("Reverse: \(self[index]!) -> \(self[self.length - 1 - index]!)")
            let temp = self[index]
            reversed[index]! = self[self.length - 1 - index]!
            reversed[self.length - 1 - index]! = temp!
        }
        return reversed
    }
}

let reversed = List<Int>(1, 1, 2, 3, 5)!.reverse()
print("Reversed -> ", terminator: "")
for index in 0...(reversed.length - 1) {
    print(reversed[index]!, terminator: ", ")
}
print()

// Problem 06: Find out whether a linked list is a palindrome.
extension List where T:Equatable {
    func isPalindrome() -> Bool {
        var isPalindrome = true
        for index in 0...((self.length / 2) - 1) {
            let front = self[index]!
            let rear = self[self.length - index - 1]!
            let matches = front == rear
            // print("\(front) - \(rear) ... \(matches)")
            isPalindrome = matches && isPalindrome
        }
        return isPalindrome
    }
}

let isPalindrome = List(1, 2, 3, 2, 1)!.isPalindrome()
print("Palindrome? -> \(isPalindrome)")
let isNotAPalindrome = List(1, 2, 8, 3, 1)!.isPalindrome()
print("Palindrome? -> \(isNotAPalindrome)")
