import Foundation

func randomOptionalInt() -> Int? {
    let randomNumber = arc4random_uniform(100)
    if randomNumber > 50 {
        return Int(randomNumber)
    } else {
        return nil
    }
}

var count: Int? // This may be nil OR an integer
count = randomOptionalInt()
if count != nil {
    print("Count was \(count!)")
} else {
    print("Count was nil.")
}
